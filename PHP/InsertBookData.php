

<?php
if (isset($_POST['BSubmit'])) {

    $Discipline = $_POST['Discipline'];
    $Branch = $_POST['Branch'];
    $BAuthor = $_POST['BAuthor'];
    $BTitle = $_POST['BTitle'];
    $BChapter = $_POST['BChapter'];
    $BYear = $_POST['BYear'];
    $BPublisher = $_POST['BPublisher'];
    $BIsbn = $_POST['BIsbn'];

    include_once('../ConDatabase/Database.php');

    $errorEmpty = false;

    if (empty($BAuthor) || empty($BTitle) || empty($BChapter) || empty($BPublisher) || empty($BIsbn) || $Discipline == "Discipline" || $BYear == "Year") {
        echo "<span class='form-error'>Please fill all required fields.</span>";
        $errorEmpty = true;
    } else {
      $sql = "Insert into Book (Discipline, Branch, Author, Title, Chapter, Year, Publisher, ISBN) values ('$Discipline','$Branch','$BAuthor','$BTitle','$BChapter','$BYear','$BPublisher','$BIsbn')";

      $result = mysqli_query($conn,$sql);

      if ($result) {
        ?>
        <span class='form-success'>Your Data has been inserted successfully.</span>";
<?php
      }
      else {
        echo mysqli_error();
      }
    }
} else {
    echo "There was no error!";
}
?>
<script>

    var d = document.getElementById('discipline');
    var b = document.getElementById('branch');

    $("#BAuthor, #BTitle, #BChapter, #BYear, #BPublisher, #BIsbn, #discipline").removeClass("input-error");
    var errorEmpty = "<?php echo $errorEmpty; ?>";

    if (errorEmpty == true) {
        $("#BAuthor, #BTitle, #BChapter, #BYear, #BPublisher, #BIsbn, #discipline").addClass("input-error");
    }
    if (errorEmpty == false) {
        $("#BAuthor, #BTitle, #BChapter, #BPublisher, #BIsbn").val("");
        $("#discipline").val("Discipline");
        $("#branch").val("Branch");
        $("#BYear").val("Year");


        b.innerHTML = "";

        if (d.value == "Discipline") {
            var optionArray = ["Branch|Branch"];
        }

        for (var option in optionArray) {
            var pair = optionArray[option].split("|");
            var newOption = document.createElement("option");
            newOption.value = pair[0];
            newOption.innerHTML = pair[1];
            b.options.add(newOption);
        }
    }




</script>
