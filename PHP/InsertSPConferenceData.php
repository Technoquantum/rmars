

<?php
if (isset($_POST['CSPSubmit'])) {

    $Discipline = $_POST['Discipline'];
    $Branch = $_POST['Branch'];
    $CSPYear = $_POST['CSPYear'];
    $CSPWebScience = $_POST['CSPWebScience'];
    $CSPScopus = $_POST['CSPScopus'];
    $CSPGoogleSch = $_POST['CSPGoogleSch'];
    $CSPIndi = $_POST['CSPIndi'];

    include_once('../ConDatabase/Database.php');

    $errorEmpty = false;

    if (empty($CSPWebScience) || empty($CSPScopus) || empty($CSPGoogleSch) || empty($CSPIndi) || $Discipline == "Discipline" || $CSPYear == "Year") {
        echo "<span class='form-error'>Please fill all required fields.</span>";
        $errorEmpty = true;
    } else {
      $sql = "Insert into publicationsubmissionyear (Discipline,Branch, CYear, PublicationTitle, CWebScience, CScopus, CGoogle, CIndiancited) values ('$Discipline','$Branch','$CSPYear', 'Conference', '$CSPWebScience','$CSPScopus','$CSPGoogleSch','$CSPIndi')";

      $result = mysqli_query($conn,$sql);

      if ($result) {
        echo "<span class='form-success'>Your Data has been inserted successfully.</span>";
      }
      else {
        echo mysqli_error($conn);
      }
    }
} else {
    echo "There was no error!";
}
?>

<script>

    var d = document.getElementById('discipline');
    var b = document.getElementById('branch');

    $("#CSPWebScience, #CSPScopus, #CSPGoogleSch, #CSPIndi, #CSPYear, #discipline").removeClass("input-error");
    var errorEmpty = "<?php echo $errorEmpty; ?>";

    if (errorEmpty == true) {
        $("#CSPWebScience, #CSPScopus, #CSPGoogleSch, #CSPIndi, #CSPYear, #discipline").addClass("input-error");
    }
    if (errorEmpty == false) {
        $("#CSPWebScience, #CSPScopus, #CSPGoogleSch, #CSPIndi, #discipline").val("");
        $("#discipline").val("Discipline");
        $("#branch").val("Branch");
        $("#CSPYear").val("Year");


        b.innerHTML = "";

        if (d.value == "Discipline") {
            var optionArray = ["Branch|Branch"];
        }

        for (var option in optionArray) {
            var pair = optionArray[option].split("|");
            var newOption = document.createElement("option");
            newOption.value = pair[0];
            newOption.innerHTML = pair[1];
            b.options.add(newOption);
        }
    }




</script>
