

<?php
if (isset($_POST['JSPSubmit'])) {

    $Discipline = $_POST['Discipline'];
    $Branch = $_POST['Branch'];
    $JSPYear = $_POST['JSPYear'];
    $JSPWebScience = $_POST['JSPWebScience'];
    $JSPScopus = $_POST['JSPScopus'];
    $JSPGoogleSch = $_POST['JSPGoogleSch'];
    $JSPIndi = $_POST['JSPIndi'];

    include_once('../ConDatabase/Database.php');

    $errorEmpty = false;

    if (empty($JSPWebScience) || empty($JSPScopus) || empty($JSPGoogleSch) || empty($JSPIndi) || $Discipline == "Discipline" || $JSPYear == "Year") {
        echo "<span class='form-error'>Please fill all required fields.</span>";
        $errorEmpty = true;
    } else {
      $sql = "Insert into publicationsubmissionyear (Discipline,Branch, JYear, PublicationTitle, JWebScience, JScopus, JGoogle, JIndiancited) values ('$Discipline','$Branch','$JSPYear', 'Journal', '$JSPWebScience','$JSPScopus','$JSPGoogleSch','$JSPIndi')";

      $result = mysqli_query($conn,$sql);

      if ($result) {
        echo "<span class='form-success'>Your Data has been inserted successfully.</span>";
      }
      else {
        echo mysqli_error($conn);
      }
    }
} else {
    echo "There was no error!";
}
?>

<script>

    var d = document.getElementById('discipline');
    var b = document.getElementById('branch');

    $("#JSPWebScience, #JSPScopus, #JSPGoogleSch, #JSPIndi, #JSPYear, #discipline").removeClass("input-error");
    var errorEmpty = "<?php echo $errorEmpty; ?>";

    if (errorEmpty == true) {
        $("#JSPWebScience, #JSPScopus, #JSPGoogleSch, #JSPIndi, #JSPYear, #discipline").addClass("input-error");
    }
    if (errorEmpty == false) {
        $("#JSPWebScience, #JSPScopus, #JSPGoogleSch, #JSPIndi, #discipline").val("");
        $("#discipline").val("Discipline");
        $("#branch").val("Branch");
        $("#JSPYear").val("Year");


        b.innerHTML = "";

        if (d.value == "Discipline") {
            var optionArray = ["Branch|Branch"];
        }

        for (var option in optionArray) {
            var pair = optionArray[option].split("|");
            var newOption = document.createElement("option");
            newOption.value = pair[0];
            newOption.innerHTML = pair[1];
            b.options.add(newOption);
        }
    }




</script>
