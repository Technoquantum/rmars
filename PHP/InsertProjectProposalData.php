

<?php
if (isset($_POST['PPsubmit'])) {

    $Discipline = $_POST['Discipline'];
    $Branch = $_POST['Branch'];
    $ProTitle = $_POST['ProTitle'];
    $PPYear = $_POST['PPYear'];
    $PrincName = $_POST['PrincName'];
    $InvesName = $_POST['InvesName'];
    $CoInvestName = $_POST['CoInvestName'];
    $FAScheme = $_POST['FAScheme'];
    $AmountNumber = $_POST['AmountNumber'];
    $PPStatus = $_POST['PPStatus'];

    include_once('../ConDatabase/Database.php');

    $errorEmpty = false;

    if (empty($ProTitle) || empty($PrincName) || empty($InvesName) || empty($CoInvestName) || empty($FAScheme) || empty($AmountNumber) || empty($PPStatus) || $Discipline == "Discipline" || $PPYear == "Year") {
        echo "<span class='form-error'>Please fill all required fields.</span>";
        $errorEmpty = true;
    } else {
        $sql = "Insert into ProjectProposal (Discipline, Branch, ProjectTitle, PYear, PrincipleName, InvestigatorName, Co_InvestigatorName, FAScheme, Amount, Status) values ('$Discipline','$Branch','$ProTitle','$PPYear','$PrincName','$InvesName','$CoInvestName','$FAScheme','$AmountNumber','$PPStatus')";

        $result = mysqli_query($conn, $sql);

        if ($result) {
            echo "<span class='form-success'>Your Data has been inserted successfully.</span>";
        } else {
            echo mysqli_error($conn);
        }
    }
} else {
    echo "There was no error!";
}
?>

<script>

    var d = document.getElementById('discipline');
    var b = document.getElementById('branch');

    $("#ProTitle, #PrincName, #InvesName, #CoInvestName, #FAScheme, #AmountNumber, #PPStatus, #PPYear, #discipline").removeClass("input-error");
    var errorEmpty = "<?php echo $errorEmpty; ?>";

    if (errorEmpty == true) {
        $("#ProTitle, #PrincName, #InvesName, #CoInvestName, #FAScheme, #AmountNumber, #PPStatus, #PPYear, #discipline").addClass("input-error");
    }
    if (errorEmpty == false) {
        $("#ProTitle, #PrincName, #InvesName, #CoInvestName, #FAScheme, #AmountNumber, #PPStatus").val("");
        $("#discipline").val("Discipline");
        $("#branch").val("Branch");
        $("#PPYear").val("Year");


        b.innerHTML = "";

        if (d.value == "Discipline") {
            var optionArray = ["Branch|Branch"];
        }

        for (var option in optionArray) {
            var pair = optionArray[option].split("|");
            var newOption = document.createElement("option");
            newOption.value = pair[0];
            newOption.innerHTML = pair[1];
            b.options.add(newOption);
        }
    }




</script>
