

<?php
if (isset($_POST['SJSubmit'])) {

    $Discipline = $_POST['Discipline'];
    $Branch = $_POST['Branch'];
    $SJAuthor = $_POST['SJAuthor'];
    $SJTitle = $_POST['SJTitle'];
    $SJJournal = $_POST['SJJournal'];
    $SJVolume = $_POST['SJVolume'];
    $SJIssueDate = $_POST['SJIssueDate'];
    $SJPage = $_POST['SJPage'];
    $SJMonth = $_POST['SJMonth'];
    $SJYear = $_POST['SJYear'];
    $SJScope = $_POST['SJScope'];
    $SJIssn = $_POST['SJIssn'];
    $SJIndex = $_POST['SJIndex'];
    $SJIndexing = $_POST['SJIndexing'];

    include_once('../ConDatabase/Database.php');

    $errorEmpty = false;

    if (empty($SJAuthor) || empty($SJTitle) || empty($SJJournal) || empty($SJVolume) || empty($SJPage) || empty($SJMonth) || $SJIssueDate == 'Day' || $Discipline == 'Discipline' || $SJYear == 'Year' || $SJScope == 'Scope' || $SJIndex == 'IU'  || empty($SJIssn)) {
        echo "<span class='form-error'>Please fill all required fields.</span>";
        $errorEmpty = true;
    } else {
      $sql = "Insert into journalsubmissionyear (Discipline, Branch, Author, Title, Journal, Volume, DAY, Page, Month, Year, Scope, ISSN, UIndex, IndexedIn ) values ('$Discipline','$Branch','$SJAuthor', '$SJTitle', '$SJJournal', '$SJVolume', '$SJIssueDate', '$SJPage', '$SJMonth', '$SJYear', '$SJScope', '$SJIssn', '$SJIndex', '$SJIndexing' )";


      $result = mysqli_query($conn,$sql);




      if ($result) {
        echo "<span class='form-success'>Your Data has been inserted successfully.</span>";
      }
      else {
        echo mysqli_error($conn);
      }
    }
} else {
    echo "There was no error!";
}
?>

<script>

   var d = document.getElementById('discipline');
    var b = document.getElementById('branch');

    $("#SJAuthor, #SJTitle, #SJJournal, #SJVolume, #SJIssueDate, #SJPage, #SJMonth, #SJYear, #SJScope, #SJIssn, #SJIndex, #discipline").removeClass("input-error");
    var errorEmpty = "<?php echo $errorEmpty; ?>";

    if (errorEmpty == true) {
        $("#SJAuthor, #SJTitle, #SJJournal, #SJVolume, #SJIssueDate, #SJPage, #SJMonth, #SJYear, #SJScope, #SJIssn, #SJIndex, #discipline").addClass("input-error");
    }
    if (errorEmpty == false) {
        $("#SJAuthor, #SJTitle, #SJJournal, #SJVolume, #SJPage, #SJIssn").val("");
        $("#discipline").val("Discipline");
        $("#branch").val("Branch");
        $("#SJIssueDate").val("Day");
        $("#SJMonth").val("Month");
        $("#SJYear").val("Year");
        $("#SJScope").val("Scope");
        $("#SJIndex").val("IU");


        b.innerHTML = "";

        if (d.value == "Discipline") {
            var optionArray = ["Branch|Branch"];
        }

        for (var option in optionArray) {
            var pair = optionArray[option].split("|");
            var newOption = document.createElement("option");
            newOption.value = pair[0];
            newOption.innerHTML = pair[1];
            b.options.add(newOption);
        }
    }




</script>
