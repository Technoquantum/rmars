<?php
 session_start();
 ?>
 <!DOCTYPE html>
 <html>
      <head>
           <title>RMARS</title>
           <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
        <link rel="stylesheet" href="CSS/style.css">

      </head>
      <body id="MainPage">
           <br />
           <div class="container text-center">

                <?php
                if(isset($_SESSION['username']))
                {
                  header('location: InsertTab.php');
                ?>

                <?php
                }
                else
                {
                ?>
                <h1>REVA Management Activity Research Service (RMARS)</h1>
                <br><br>
                <div class="container">

                    <!--Login Link-->
                    <div class="col-sm-6"  id="loginLink">
                        <a href="#"  class="LSbtn" data-toggle="modal" data-target="#loginModal">Login</a>
                    </div>

                    <!--Signup Link-->
                    <div class="col-sm-6" id="signupLink">
                        <a href="#" class="LSbtn" >Signup</a>
                    </div>

                </div>
                <?php
                }
                ?>
           </div>
           <br />
      </body>
 </html>
 <div id="loginModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
   <!-- Modal content-->
           <div class="modal-content">
                <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title">Login</h4>
                </div>
                <div class="modal-body">
                     <label>Username</label>
                     <input type="text" name="username" id="username" class="form-control" />
                     <br />
                     <label>Password</label>
                     <input type="password" name="password" id="password" class="form-control" />
                     <br />
                     <button type="button" name="login_button" id="login_button" class="btn btn-warning">Login</button>
                </div>
           </div>
      </div>
 </div>
 <script>
 $(document).ready(function(){
      $('#login_button').click(function(){
           var username = $('#username').val();
           var password = $('#password').val();
           if(username != '' && password != '')
           {
                $.ajax({
                     url:"LoginAction.php",
                     method:"POST",
                     data: {username:username, password:password},
                     success:function(data)
                     {
                          //alert(data);
                          if(data == 'No')
                          {
                               alert("Wrong Data");
                          }
                          else
                          {
                               $('#loginModal').hide();
                               location.reload();
                          }
                     }
                });
           }
           else
           {
                alert("Both Fields are required");
           }
      });
      $('#logout').click(function(){
           var action = "logout";
           $.ajax({
                url:"LoginAction.php",
                method:"POST",
                data:{action:action},
                success:function()
                {
                     location.reload();
                }
           });
      });
 });
 </script>
