CREATE DATABASE  IF NOT EXISTS `rmars` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `rmars`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: rmars
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `Discipline` varchar(30) NOT NULL,
  `Branch` varchar(60) NOT NULL,
  `Author` varchar(50) DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `Chapter` varchar(50) DEFAULT NULL,
  `Year` varchar(5) DEFAULT NULL,
  `Publisher` varchar(60) DEFAULT NULL,
  `ISBN` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES ('Discipline','Branch','s','s','s','2018','s','s'),('SCIENCE and TECHNOLOGY','BIO-CHEMISTRY','a','a','a','2017','a','a'),('SCIENCE and TECHNOLOGY','STATISTICS','w','w','w','2014','w','w'),('ARTS, HUMANITIES and EDUCATION','KANNADA','d','d','d','2016','d','d'),('ENGINEERING and TECHNOLOGY','COMPUTER and TECHNOLOGY','d','d','d','2017','d','d'),('SCIENCE and TECHNOLOGY','PHYSICAL SCIENCE and COMPUTER APPLICATION','s','s','s','2017','s','s'),('ARTS, HUMANITIES and EDUCATION','KANNADA','s','s','s','2018','s','s'),('Discipline','Branch','','','','','',''),('Discipline','Branch','','','','','',''),('Discipline','Branch','','','','','',''),('Discipline','Branch','','','','','',''),('Discipline','Branch','','','','','',''),('Discipline','Branch','','','','','',''),('Discipline','Branch','','','','','',''),('Discipline','Branch','','','','','',''),('Discipline','Branch','','','','','',''),('Discipline','Branch','','','','','',''),('Discipline','Branch','','','','','',''),('SCIENCE and TECHNOLOGY','PHYSICAL SCIENCE and COMPUTER APPLICATION','','','','','',''),('Discipline','Branch','','','','','',''),('Discipline','Branch','','','','','',''),('ENGINEERING and TECHNOLOGY','CIVIL ENGINEERING','','','','','','');
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacthelp`
--

DROP TABLE IF EXISTS `contacthelp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacthelp` (
  `Discipline` varchar(30) NOT NULL,
  `Branch` varchar(60) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Organization` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacthelp`
--

LOCK TABLES `contacthelp` WRITE;
/*!40000 ALTER TABLE `contacthelp` DISABLE KEYS */;
INSERT INTO `contacthelp` VALUES ('LEGAL STUDIES','NO BRANCH','s','s'),('SCIENCE and TECHNOLOGY','PHYSICAL SCIENCE and COMPUTER APPLICATION','a','a');
/*!40000 ALTER TABLE `contacthelp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facultynotyetregistered`
--

DROP TABLE IF EXISTS `facultynotyetregistered`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facultynotyetregistered` (
  `Discipline` varchar(30) NOT NULL,
  `Branch` varchar(60) NOT NULL,
  `FacultyName` varchar(50) NOT NULL,
  `Day` varchar(3) DEFAULT NULL,
  `Month` varchar(14) DEFAULT NULL,
  `Year` varchar(5) DEFAULT NULL,
  `Status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facultynotyetregistered`
--

LOCK TABLES `facultynotyetregistered` WRITE;
/*!40000 ALTER TABLE `facultynotyetregistered` DISABLE KEYS */;
INSERT INTO `facultynotyetregistered` VALUES ('ARTS, HUMANITIES and EDUCATION','ENGLISH','','12','9','Year',''),('ENGINEERING and TECHNOLOGY','COMPUTER and TECHNOLOGY','w','12','9','Year',''),('ENGINEERING and TECHNOLOGY','ELECTRONIC and COMMUNICATION ENGINEERING','e','12','12','Year',''),('SCIENCE and TECHNOLOGY','PHYSICAL SCIENCE and COMPUTER APPLICATION','w','12','12','Year',''),('Discipline','Branch','d','7','8','Year',''),('ENGINEERING and TECHNOLOGY','CIVIL ENGINEERING','f','10','5','Year',''),('SCIENCE and TECHNOLOGY','PHYSICAL SCIENCE and COMPUTER APPLICATION','g','12','9','Year',''),('SCIENCE and TECHNOLOGY','MATHEMATICS','s','7','9','Year',''),('Discipline','Branch','','Day','Month','Year',''),('ARTS, HUMANITIES and EDUCATION','ENGLISH','f','8','3','Year',''),('Discipline','Branch','f','20','12','Year',''),('SCIENCE and TECHNOLOGY','PHYSICAL SCIENCE and COMPUTER APPLICATION','C','14','8','2015','C'),('ENGINEERING and TECHNOLOGY','ELECTRICAL and ELECTRONICS ENGINEERING','T','19','11','2016','T');
/*!40000 ALTER TABLE `facultynotyetregistered` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facultypublicationdata`
--

DROP TABLE IF EXISTS `facultypublicationdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facultypublicationdata` (
  `Discipline` varchar(30) NOT NULL,
  `Branch` varchar(60) NOT NULL,
  `FacultyName` varchar(50) NOT NULL,
  `Day` varchar(4) DEFAULT NULL,
  `Month` varchar(4) DEFAULT NULL,
  `Year` varchar(5) DEFAULT NULL,
  `Status` varchar(50) DEFAULT NULL,
  `RDay` varchar(4) DEFAULT NULL,
  `RMonth` varchar(4) DEFAULT NULL,
  `RYear` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facultypublicationdata`
--

LOCK TABLES `facultypublicationdata` WRITE;
/*!40000 ALTER TABLE `facultypublicationdata` DISABLE KEYS */;
INSERT INTO `facultypublicationdata` VALUES ('LEGAL STUDIES','NO BRANCH','V','18','10','2017','V','18','11','2017');
/*!40000 ALTER TABLE `facultypublicationdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `journalsubmissionyear`
--

DROP TABLE IF EXISTS `journalsubmissionyear`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `journalsubmissionyear` (
  `Discipline` varchar(30) NOT NULL,
  `Branch` varchar(60) NOT NULL,
  `Author` varchar(50) DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `Journal` varchar(300) DEFAULT NULL,
  `Volume` varchar(30) DEFAULT NULL,
  `DAY` varchar(4) DEFAULT NULL,
  `PAGE` varchar(20) DEFAULT NULL,
  `MONTH` varchar(4) DEFAULT NULL,
  `YEAR` varchar(5) DEFAULT NULL,
  `Scope` varchar(16) DEFAULT NULL,
  `ISSN` varchar(40) DEFAULT NULL,
  `UIndex` varchar(10) DEFAULT NULL,
  `IndexedIn` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `journalsubmissionyear`
--

LOCK TABLES `journalsubmissionyear` WRITE;
/*!40000 ALTER TABLE `journalsubmissionyear` DISABLE KEYS */;
INSERT INTO `journalsubmissionyear` VALUES ('LEGAL STUDIES','NO BRANCH','cxvxcv',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('LEGAL STUDIES','NO BRANCH','zxczxc','zxczxc',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('LEGAL STUDIES','NO BRANCH','zxcc','zxc','zxcz',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('ENGINEERING and TECHNOLOGY','CIVIL ENGINEERING','zxc','zxc','zxc',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('LEGAL STUDIES','NO BRANCH','cv','xcv','xcv','xcv',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('ARTS, HUMANITIES and EDUCATION','KANNADA','zxc','zxc','zxc','zxc','19',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('ENGINEERING and TECHNOLOGY','CIVIL ENGINEERING','zxc','zxc','zxc','zxc','18','zxc',NULL,NULL,NULL,NULL,NULL,NULL),('ARTS, HUMANITIES and EDUCATION','KANNADA','zxc','zxc','zxc','zxc','16','xzc','10',NULL,NULL,NULL,NULL,NULL),('ENGINEERING and TECHNOLOGY','CIVIL ENGINEERING','zxc','zxc','zxc','zxc','11','zxc','9','2016',NULL,NULL,NULL,NULL),('SCIENCE and TECHNOLOGY','PHYSICAL SCIENCE and COMPUTER APPLICATION','cxzc','zxc','zxc','zxc','17','xc','2','2017','International',NULL,NULL,NULL),('SCIENCE and TECHNOLOGY','PHYSICAL SCIENCE and COMPUTER APPLICATION','zxc','zxc','xzc','zxc','1','xzc','1','2018','International','',NULL,NULL),('SCIENCE and TECHNOLOGY','PHYSICAL SCIENCE and COMPUTER APPLICATION','zxc','zxc','xzc','zxc','1','xzc','2','2018','International','zxczxc',NULL,NULL),('Discipline','Branch','','','','','3','','2','Year','National','',NULL,NULL),('Discipline','Branch','','','','','3','','2','Year','International','',NULL,NULL),('Discipline','Branch','','','','','2','','3','2017','International','',NULL,NULL),('Discipline','Branch','asd','asd','asd','asd','2','sad','2','2018','International','asd','Indexed',NULL),('ARTS, HUMANITIES and EDUCATION','KANNADA','zxc','zxc','zxc','zxc','3','zxc','3','2017','National','zxc','Indexed',NULL),('Discipline','Branch','zxc','zxc','zxc','zxc','19','zxc','9','2018','National','xzc','Indexed','Web Of Science'),('Discipline','Branch','zxc','zxc','zxc','zxc','3','xzc','2','2017','International','zxc','Unindexed','IUI'),('ARTS, HUMANITIES and EDUCATION','KANNADA','zxc','xzc','zxc','zxc','2','zxc','3','2017','National','zxc','Unindexed','No where'),('ARTS, HUMANITIES and EDUCATION','KANNADA','zxc','xzc','zxc','zxc','2','zxc','3','2017','National','zxc','Indexed','Indiancited'),('ARTS, HUMANITIES and EDUCATION','KANNADA','zxc','xzc','zxc','zxc','2','zxc','3','2017','National','zxc','Indexed','No where'),('Discipline','Branch','','','','','3','','1','2018','International','','Indexed','UGC');
/*!40000 ALTER TABLE `journalsubmissionyear` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `Name` varchar(50) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  PRIMARY KEY (`Username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES ('Aman Singh','amansingh12091998@gmail.com','aman');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projectproposal`
--

DROP TABLE IF EXISTS `projectproposal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projectproposal` (
  `Discipline` varchar(30) NOT NULL,
  `Branch` varchar(60) NOT NULL,
  `ProjectTitle` varchar(70) NOT NULL,
  `PYear` varchar(5) NOT NULL,
  `PrincipleName` varchar(40) DEFAULT NULL,
  `InvestigatorName` varchar(50) DEFAULT NULL,
  `Co_InvestigatorName` varchar(50) DEFAULT NULL,
  `FAScheme` varchar(80) DEFAULT NULL,
  `Amount` varchar(10) DEFAULT NULL,
  `Status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projectproposal`
--

LOCK TABLES `projectproposal` WRITE;
/*!40000 ALTER TABLE `projectproposal` DISABLE KEYS */;
INSERT INTO `projectproposal` VALUES ('ENGINEERING and TECHNOLOGY','CIVIL ENGINEERING','d','2018','d','d','d','d','4','d'),('ENGINEERING and TECHNOLOGY','CIVIL ENGINEERING','r','2017','r','r','r','r','5','r'),('ENGINEERING and TECHNOLOGY','CIVIL ENGINEERING','r','2017','r','r','r','r','5','r'),('SCIENCE and TECHNOLOGY','PHYSICAL SCIENCE and COMPUTER APPLICATION','r','2017','r','r','r','r','5','r'),('ENGINEERING and TECHNOLOGY','ELECTRONIC and COMMUNICATION ENGINEERING','r','2017','r','r','r','r','5','r'),('SCIENCE and TECHNOLOGY','PHYSICAL SCIENCE and COMPUTER APPLICATION','r','2017','r','r','r','r','5','r'),('SCIENCE and TECHNOLOGY','PHYSICAL SCIENCE and COMPUTER APPLICATION','r','2018','r','r','r','r','4','r'),('SCIENCE and TECHNOLOGY','PHYSICAL SCIENCE and COMPUTER APPLICATION','e','2018','e','e','e','e','3','e'),('ENGINEERING and TECHNOLOGY','COMPUTER and TECHNOLOGY','t','2018','t','t','t','t','14000.30','e'),('ENGINEERING and TECHNOLOGY','CIVIL ENGINEERING','s','2018','s','ss','s','s','3','s'),('SCIENCE and TECHNOLOGY','PHYSICAL SCIENCE and COMPUTER APPLICATION','x','2017','x','x','x','x','2','x'),('ENGINEERING and TECHNOLOGY','COMPUTER and TECHNOLOGY','n','2018','n','n','n','n','4','m'),('ENGINEERING and TECHNOLOGY','MECHANICAL ENGINEERING','a','2017','a','a','a','a','3','a'),('LEGAL STUDIES','NO BRANCH','b','2018','b','b','b','b','5','b'),('ARTS, HUMANITIES and EDUCATION','ENGLISH','S','2017','S','S','S','S','2','S'),('Discipline','Branch','','Year','','','','','','');
/*!40000 ALTER TABLE `projectproposal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publicationsubmissionyear`
--

DROP TABLE IF EXISTS `publicationsubmissionyear`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publicationsubmissionyear` (
  `Discipline` varchar(30) NOT NULL,
  `Branch` varchar(60) NOT NULL,
  `PublicationTitle` varchar(12) NOT NULL,
  `WebScience` varchar(100) DEFAULT NULL,
  `Scopus` varchar(100) DEFAULT NULL,
  `Google` varchar(100) DEFAULT NULL,
  `Indication` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publicationsubmissionyear`
--

LOCK TABLES `publicationsubmissionyear` WRITE;
/*!40000 ALTER TABLE `publicationsubmissionyear` DISABLE KEYS */;
INSERT INTO `publicationsubmissionyear` VALUES ('SCIENCE and TECHNOLOGY','PHYSICAL SCIENCE and COMPUTER APPLICATION','Journal','dsadasd','asdasd','asdasd','sadasd'),('ARTS, HUMANITIES and EDUCATION','ENGLISH','Journal','sadasd','asdasd','asdasd','asdasdasd'),('ARTS, HUMANITIES and EDUCATION','HINDI','Journal','zxc','xzc','zxc','zxzx'),('SCIENCE and TECHNOLOGY','PHYSICAL SCIENCE and COMPUTER APPLICATION','Conference','asd','asd','asd','asd'),('SCIENCE and TECHNOLOGY','STATISTICS','Journal','wqe','qwe','qwe','qwe'),('SCIENCE and TECHNOLOGY','CHEMISTRY','Journal','zxc','xczx','zxczc','xzczxc'),('SCIENCE and TECHNOLOGY','STATISTICS','Conference','zxczxc','zxczxc','zxczcx','zxczxc'),('ARTS, HUMANITIES and EDUCATION','ENGLISH','Journal','fgdg','dfgdfg','ddfg','dfgdfg'),('LEGAL STUDIES','NO BRANCH','Conference','dfgdf','dfg','dfg','dfg'),('Discipline','Branch','Journal','','','',''),('Discipline','Branch','Conference','','','',''),('SCIENCE and TECHNOLOGY','CHEMISTRY','Conference','d','d','d','d');
/*!40000 ALTER TABLE `publicationsubmissionyear` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `universityhelp`
--

DROP TABLE IF EXISTS `universityhelp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `universityhelp` (
  `Discipline` varchar(30) NOT NULL,
  `Branch` varchar(60) NOT NULL,
  `Requirements` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `universityhelp`
--

LOCK TABLES `universityhelp` WRITE;
/*!40000 ALTER TABLE `universityhelp` DISABLE KEYS */;
INSERT INTO `universityhelp` VALUES ('SCIENCE and TECHNOLOGY','CHEMICAL and BIOLOGICAL SCIENCE','asascasc'),('ARTS, HUMANITIES and EDUCATION','EDUCATION','qwewe'),('ENGINEERING and TECHNOLOGY','CIVIL ENGINEERING','xczxczxc'),('ARTS, HUMANITIES and EDUCATION','KANNADA','advsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdvadvsvvsadsdvsdvsdv'),('Discipline','Branch','zxczxc'),('Discipline','Branch','vcvxcvxcv'),('ENGINEERING and TECHNOLOGY','CIVIL ENGINEERING','ascasc'),('LEGAL STUDIES','NO BRANCH','ascacas'),('ARTS, HUMANITIES and EDUCATION','KANNADA','acasc'),('Discipline','Branch','acacas'),('SCIENCE and TECHNOLOGY','PHYSICAL SCIENCE and COMPUTER APPLICATION','zxczc'),('Discipline','Branch','sddssd'),('Discipline','Branch','ascasc'),('Discipline','Branch','sacasc'),('Discipline','Branch','asasdasd'),('Discipline','Branch','sacascas'),('Discipline','Branch','zxczxczxc');
/*!40000 ALTER TABLE `universityhelp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'rmars'
--

--
-- Dumping routines for database 'rmars'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-14 20:11:14
