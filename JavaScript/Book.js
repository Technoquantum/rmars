$(document).ready(function(){
  $("#BForm").submit(function(event){
    event.preventDefault();
    /*var Discipline = document.getElementById("discipline").value;
    var Branch = document.getElementById("branch").value;
    var ProTitle = document.getElementById("ProTitle").value;
    var PPYear = document.getElementById("PPYear").value;
    var PrincName = document.getElementById("PrincName").value;
    var InvesName = document.getElementById("InvesName").value;
    var CoInvestName = document.getElementById("CoInvestName").value;
    var FAScheme = document.getElementById("FAScheme").value;
    var AmountNumber = document.getElementById("AmountNumber").value;
    var PPStatus = document.getElementById("PPStatus").value;*/
    var Discipline = $("#discipline").val();
    var Branch = $("#branch").val();
    var BAuthor = $("#BAuthor").val();
    var BTitle = $("#BTitle").val();
    var BChapter = $("#BChapter").val();
    var BYear = $("#BYear").val();
    var BPublisher = $("#BPublisher").val();
    var BIsbn = $("#BIsbn").val();
    var BSubmit = $("#BSubmit").val();
    $("#BMessage").load("PHP/InsertBookData.php", {
      Discipline:Discipline,
      Branch:Branch,
      BAuthor:BAuthor,
      BTitle:BTitle,
      BChapter:BChapter,
      BYear:BYear,
      BPublisher:BPublisher,
      BIsbn:BIsbn,
      BSubmit:BSubmit
    });
  });
});
