$(document).ready(function(){
  $("#SJForm").submit(function(event){
    event.preventDefault();
    var Discipline = $("#discipline").val();
    var Branch = $("#branch").val();
    var SJAuthor = $("#SJAuthor").val();
    var SJTitle = $("#SJTitle").val();
    var SJJournal = $("#SJJournal").val();
    var SJVolume = $("#SJVolume").val();
    var SJIssueDate = $("#SJIssueDate").val();
    var SJPage = $("#SJPage").val();
    var SJMonth = $("#SJMonth").val();
    var SJYear = $("#SJYear").val();
    var SJScope = $("#SJScope").val();

    var SJIssn = $("#SJIssn").val();
    var SJIndex = $("#SJIndex").val();
    var SJIndexing = $("#SJIndexing").val();
    var SJSubmit = $("#SJSubmit").val();
    $("#SJMessage").load("PHP/InsertSubmissionJournal.php", {
      Discipline:Discipline,
      Branch:Branch,
      SJAuthor:SJAuthor,
      SJTitle:SJTitle,
      SJJournal:SJJournal,
      SJVolume:SJVolume,
      SJIssueDate:SJIssueDate,
      SJPage:SJPage,
      SJMonth:SJMonth,
      SJYear:SJYear,
      SJScope:SJScope,

      SJIssn:SJIssn,
      SJIndex:SJIndex,
      SJIndexing:SJIndexing,
      SJSubmit:SJSubmit
    });
  });
});
