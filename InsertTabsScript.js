
//-----------------------------------------DISCIPLINE & BRANCH COMBOBOX CODING--------------------------------------


function populate(d, b) {
    var d = document.getElementById(d);
    var b = document.getElementById(b);

    b.innerHTML = "";

    if (d.value == "Discipline") {
        var optionArray = ["Branch|Branch"];
    } else if (d.value == "ENGINEERING and TECHNOLOGY") {
        var optionArray = ["CIVIL ENGINEERING|CIVIL ENGINEERING",
            "COMPUTER and TECHNOLOGY|COMPUTER & TECHNOLOGY",
            "ELECTRICAL and ELECTRONICS ENGINEERING|ELECTRICAL & ELECTRONICS ENGINEERING",
            "ELECTRONIC and COMMUNICATION ENGINEERING|ELECTRONIC & COMMUNICATION ENGINEERING",
            "MECHANICAL ENGINEERING|MECHANICAL ENGINEERING",
            "ARCHITECTURE ENGINEERING|ARCHITECTURE ENGINEERING"];
    } else if (d.value == "SCIENCE and TECHNOLOGY") {
        var optionArray = ["PHYSICAL SCIENCE and COMPUTER APPLICATION|PHYSICAL SCIENCE & COMPUTER APPLICATION",
            "CHEMICAL and BIOLOGICAL SCIENCE|CHEMICAL & BIOLOGICAL SCIENCE",
            "BIO-CHEMISTRY|BIO-CHEMISTRY",
            "CHEMISTRY|CHEMISTRY",
            "MATHEMATICS|MATHEMATICS",
            "PHYSICS|PHYSICS",
            "STATISTICS|STATISTICS"];
    } else if (d.value == "ARTS, HUMANITIES and EDUCATION") {
        var optionArray = ["KANNADA|KANNADA",
            "ENGLISH|ENGLISH",
            "HINDI|HINDI",
            "EDUCATION|EDUCATION"];
    } else if (d.value == "LEGAL STUDIES") {
        var optionArray = ["NO BRANCH|NO BRANCH"];
    } else if (d.value == "COMMERCE and MANAGEMENT") {
      var optionArray = ["COMMERCE|COMMERCE",
          "ECONOMICS|ECONOMICS",
          "LIBRARY and INFORMATION SCIENCE|LIBRARY & INFORMATION SCIENCE",
          "MANAGEMENT STUDIES|MANAGEMENT STUDIES"];
    }





    for (var option in optionArray) {
        var pair = optionArray[option].split("|");
        var newOption = document.createElement("option");
        newOption.value = pair[0];
        newOption.innerHTML = pair[1];
        b.options.add(newOption);
    }
}











//-----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------


//Function of Project Proposal

function ProjectProposal() {




  $(document).ready(function(){
    $("form").submit(function(event){
      event.preventDefault();
      /*var Discipline = document.getElementById("discipline").value;
      var Branch = document.getElementById("branch").value;
      var ProTitle = document.getElementById("ProTitle").value;
      var PPYear = document.getElementById("PPYear").value;
      var PrincName = document.getElementById("PrincName").value;
      var InvesName = document.getElementById("InvesName").value;
      var CoInvestName = document.getElementById("CoInvestName").value;
      var FAScheme = document.getElementById("FAScheme").value;
      var AmountNumber = document.getElementById("AmountNumber").value;
      var PPStatus = document.getElementById("PPStatus").value;*/
      var Discipline = $("#discipline").val();
      var Branch = $("#branch").val();
      var ProTitle = $("#ProTitle").val();
      var PPYear = $("#PPYear").val();
      var PrincName = $("#PrincName").val();
      var InvesName = $("#InvesName").val();
      var CoInvestName = $("#CoInvestName").val();
      var FAScheme = $("#FAScheme").val();
      var AmountNumber = $("#AmountNumber").val();
      var PPStatus = $("#PPStatus").val();
      var submit = $("#PPSubmit").val();
      $("#Message").load("InsertProjectProposalData.php", {
        Discipline:Discipline,
        Branch:Branch,
        ProTitle:ProTitle,
        PPYear:PPYear,
        PrincName:PrincName,
        InvesName:InvesName,
        CoInvestName:CoInvestName,
        FAScheme:FAScheme,
        AmountNumber:AmountNumber,
        PPStatus:PPStatus,
        submit:submit
      });
    });
  });

    //By Name we can initialize the Input Values
    //var vp = $("input[name=ProTitle]").val();
    //var vm = $("input[name=PrincName]").val();

    /*var Discipline = document.getElementById("discipline").value;
    var Branch = document.getElementById("branch").value;
    var ProTitle = document.getElementById("ProTitle").value;
    var PPYear = document.getElementById("PPYear").value;
    var PrincName = document.getElementById("PrincName").value;
    var InvesName = document.getElementById("InvesName").value;
    var CoInvestName = document.getElementById("CoInvestName").value;
    var FAScheme = document.getElementById("FAScheme").value;
    var AmountNumber = document.getElementById("AmountNumber").value;
    var PPStatus = document.getElementById("PPStatus").value;




    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", "InsertProjectProposalData.php?Discipline=" + Discipline + "&Branch=" + Branch + "&ProTitle=" + ProTitle
            + "&PPYear=" + PPYear + "&PrincName=" + PrincName + "&InvesName=" + InvesName + "&CoInvestName=" + CoInvestName
            + "&FAScheme=" + FAScheme + "&AmountNumber=" + AmountNumber + "&PPStatus=" + PPStatus, false);
    xmlhttp.send(null);

    document.getElementById("Message").innerHTML = xmlhttp.responseText;

    $("select#discipline").val("Discipline");
    $("select#branch").val("Branch");
    $("input#ProTitle").val('');
    $("select#PPYear").val("Year");
    $("input#PrincName").val('');
    $("input#InvesName").val('');
    $("input#CoInvestName").val('');
    $("input#FAScheme").val('');
    $("input#AmountNumber").val('');
    $("input#PPStatus").val('');


    //By Name we can use that to accept the values
    //$("input[name=ProTitle]").val('');
    //$("input[name=PrincName]").val('');*/

}




//-----------------------------------------------------------------------------------------------------------------------------------



//Function of FACULTY WHO ARE YET TO Register

function FacultyNotRegister() {
    var Discipline = document.getElementById("discipline").value;
    var Branch = document.getElementById("branch").value;
    var FNRFaculty = document.getElementById("FNRFaculty").value;
    var FNRDay = document.getElementById("FNRDay").value;
    var FNRMonth = document.getElementById("FNRMonth").value;
    var FNRYear = document.getElementById("FNRYear").value;
    var FNRStatus = document.getElementById("FNRStatus").value;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", "InsertFacultyNotRegisteredData.php?Discipline=" + Discipline + "&Branch=" + Branch + "&FNRFaculty=" + FNRFaculty
            + "&FNRDay=" + FNRDay + "&FNRMonth=" + FNRMonth + "&FNRYear=" + FNRYear + "&FNRStatus=" + FNRStatus, false);
    xmlhttp.send(null);

    document.getElementById("FNRMessage").innerHTML = xmlhttp.responseText;

    //Clearing Fields
    $("select#discipline").val("Discipline");
    $("select#branch").val("Branch");
    $("input#FNRFaculty").val('');
    $("select#FNRDay").val("Day");
    $("select#FNRMonth").val("Month");
    $("select#FNRYear").val("Year");
    $("input#FNRStatus").val('');
}



//-----------------------------------------------------------------------------------------------------------------------------------



//Function of Faculty Members for Publication

  function FacultyPublication(){
    var Discipline = document.getElementById("discipline").value;
    var Branch = document.getElementById("branch").value;
    var FMPFaculty = document.getElementById("FMPFaculty").value;
    var FMPDay = document.getElementById("FMPDay").value;
    var FMPMonth = document.getElementById("FMPMonth").value;
    var FMPYear = document.getElementById("FMPYear").value;
    var FMPStatus = document.getElementById("FMPStatus").value;
    var FMPRDay = document.getElementById("FMPRDay").value;
    var FMPRMonth = document.getElementById("FMPRMonth").value;
    var FMPRYear = document.getElementById("FMPRYear").value;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", "InsertFacultyPublicationData.php?Discipline=" + Discipline + "&Branch=" + Branch + "&FMPFaculty=" + FMPFaculty
            + "&FMPDay=" + FMPDay + "&FMPMonth=" + FMPMonth + "&FMPYear=" + FMPYear + "&FMPRDay=" + FMPRDay + "&FMPRMonth=" + FMPRMonth +
             "&FMPRYear=" + FMPRYear + "&FMPStatus=" + FMPStatus,false);

    xmlhttp.send(null);

    document.getElementById("FMPMessage").innerHTML = xmlhttp.responseText;

    //Clearing Fields
    $("select#discipline").val("Discipline");
    $("select#branch").val("Branch");
    $("input#FMPFaculty").val('');
    $("select#FMPDay").val("Day");
    $("select#FMPMonth").val("Month");
    $("select#FMPYear").val("Year");
    $("input#FMPStatus").val('');
    $("select#FMPRDay").val("Day");
    $("select#FMPRMonth").val("Month");
    $("select#FMPRYear").val("Year");
  }

//-----------------------------------------------------------------------------------------------------------------------------------

  //Function of University Help

  function UniversityHelp() {
    var Discipline = document.getElementById("discipline").value;
    var Branch = document.getElementById("branch").value;
    var UHRequirement = document.getElementById("UHRequirement").value;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", "InsertUniversityHelpData.php?Discipline=" + Discipline + "&Branch=" + Branch + "&UHRequirement=" + UHRequirement,false);

    xmlhttp.send(null);

    document.getElementById("UHMessage").innerHTML = xmlhttp.responseText;

    //Clearing Fields
    $("select#discipline").val("Discipline");
    $("select#branch").val("Branch");
    $("textarea#UHRequirement").val('');
  }

  //-----------------------------------------------------------------------------------------------------------------------------------

    //Function of Contact Help

    function ContactHelp() {
      var Discipline = document.getElementById("discipline").value;
      var Branch = document.getElementById("branch").value;
      var CHName = document.getElementById("CHName").value;
      var CHOrg = document.getElementById("CHOrg").value;

      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open("GET", "InsertContactHelpData.php?Discipline=" + Discipline + "&Branch=" + Branch + "&CHName=" + CHName + "&CHOrg=" + CHOrg,false);

      xmlhttp.send(null);

      document.getElementById("CHMessage").innerHTML = xmlhttp.responseText;

      //Clearing Fields
      $("select#discipline").val("Discipline");
      $("select#branch").val("Branch");
      $("input#CHName").val('');
      $("input#CHOrg").val('');
    }

    //-----------------------------------------------------------------------------------------------------------------------------------

      //Function of Publication Submission (Journal)

     function SPSubmitJournal(){
        var Discipline = document.getElementById("discipline").value;
        var Branch = document.getElementById("branch").value;
        var JSPYear = document.getElementById("JSPYear").value;
        var JSPWebScience = document.getElementById("JSPWebScience").value;
        var JSPScopus = document.getElementById("JSPScopus").value;
        var JSPGoogleSch = document.getElementById("JSPGoogleSch").value;
        var JSPIndi = document.getElementById("JSPIndi").value;

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", "InsertSPJournalData.php?Discipline=" + Discipline + "&Branch=" + Branch + "&JSPYear=" + JSPYear +
             "&JSPWebScience=" + JSPWebScience + "&JSPScopus=" + JSPScopus + "&JSPGoogleSch=" + JSPGoogleSch + "&JSPIndi=" + JSPIndi,false);

        xmlhttp.send(null);

        document.getElementById("JSPMessage").innerHTML = xmlhttp.responseText;

        //Clearing Fields
        $("select#discipline").val("Discipline");
        $("select#branch").val("Branch");
        $("input#JSPWebScience").val('');
        $("input#JSPScopus").val('');
        $("input#JSPGoogleSch").val('');
        $("input#JSPIndi").val('');
      }

      //-----------------------------------------------------------------------------------------------------------------------------------

        //Function of Publication Submission (Conference)

        function SPSubmitConference(){
          var Discipline = document.getElementById("discipline").value;
          var Branch = document.getElementById("branch").value;
          var CSPYear = document.getElementById("CSPYear").value;
          var CSPWebScience = document.getElementById("CSPWebScience").value;
          var CSPScopus = document.getElementById("CSPScopus").value;
          var CSPGoogleSch = document.getElementById("CSPGoogleSch").value;
          var CSPIndi = document.getElementById("CSPIndi").value;

          var xmlhttp = new XMLHttpRequest();
          xmlhttp.open("GET", "InsertSPConferenceData.php?Discipline=" + Discipline + "&Branch=" + Branch + "&CSPYear=" + CSPYear +"&CSPWebScience=" + CSPWebScience + "&CSPScopus=" + CSPScopus + "&CSPGoogleSch=" + CSPGoogleSch + "&CSPIndi=" + CSPIndi,false);

          xmlhttp.send(null);

          document.getElementById("CSPMessage").innerHTML = xmlhttp.responseText;

          //Clearing Fields
          $("select#discipline").val("Discipline");
          $("select#branch").val("Branch");
          $("input#CSPWebScience").val('');
          $("input#CSPScopus").val('');
          $("input#CSPGoogleSch").val('');
          $("input#CSPIndi").val('');
          $("select#CSPYear").val("Year");
        }

//-----------------------------------------------------------------------------------------------------------------------------------

          //Function of Book

          function Book() {
            var Discipline = document.getElementById("discipline").value;
            var Branch = document.getElementById("branch").value;
            var BAuthor = document.getElementById("BAuthor").value;
            var BTitle = document.getElementById("BTitle").value;
            var BChapter = document.getElementById("BChapter").value;
            var BYear = document.getElementById("BYear").value;
            var BPublisher = document.getElementById("BPublisher").value;
            var BIsbn = document.getElementById("BIsbn").value;


            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("GET", "InsertBookData.php?Discipline=" + Discipline + "&Branch=" + Branch + "&BAuthor=" + BAuthor + "&BTitle=" + BTitle + "&BChapter=" + BChapter + "&BYear=" + BYear + "&BPublisher=" + BPublisher + "&BIsbn=" + BIsbn,false);

            xmlhttp.send(null);

            document.getElementById("BMessage").innerHTML = xmlhttp.responseText;


            //Clearing Fields
            $("select#discipline").val("Discipline");
            $("select#branch").val("Branch");
            $("input#BAuthor").val('');
            $("input#BTitle").val('');
            $("input#BChapter").val('');
            $("select#BYear").val("Year");
            $("input#BPublisher").val('');
            $("input#BIsbn").val('');
          }

//-----------------------------------------------------------------------------------------------------------------------------------
        //Function of Submission of Journal (Per Year)

        function SJJournall() {
          var Discipline = document.getElementById("discipline").value;
          var Branch = document.getElementById("branch").value;
          var SJAuthor = document.getElementById("SJAuthor").value;
          var SJTitle = document.getElementById("SJTitle").value;
          var SJJournal = document.getElementById("SJJournal").value;
          var SJVolume = document.getElementById("SJVolume").value;
          var SJIssueDate = document.getElementById("SJIssueDate").value;
          var SJPage = document.getElementById("SJPage").value;
          var SJMonth = document.getElementById("SJMonth").value;
          var SJYear = document.getElementById("SJYear").value;
          var SJScope = document.getElementById("SJScope").value;
          var SJIssn = document.getElementById("SJIssn").value;
          var SJIndex = document.getElementById("SJIndex").value;
          var SJIndexing = document.getElementById("SJIndexing").value;

          var xmlhttp = new XMLHttpRequest();
          xmlhttp.open("GET", "InsertSJJournalData.php?Discipline=" + Discipline + "&Branch=" + Branch + "&SJAuthor=" + SJAuthor + "&SJTitle=" + SJTitle + "&SJJournal=" + SJJournal + "&SJVolume=" + SJVolume + "&SJIssueDate=" + SJIssueDate + "&SJPage=" + SJPage + "&SJMonth=" + SJMonth + "&SJYear=" + SJYear + "&SJScope=" + SJScope + "&SJIssn=" + SJIssn + "&SJIndex=" + SJIndex + "&SJIndexing=" + SJIndexing,false);

          xmlhttp.send(null);

          document.getElementById("SJMessage").innerHTML = xmlhttp.responseText;
        }










        //-----------------------------------------------------------------------------------------------------------------------------------
