
<!DOCTYPE html>
<html>
    <head>
        <title>REVA | Showing Data</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />

        <link rel="stylesheet" href="ShowTableStyle.css">
        <script type="text/javascript" src="InsertTabsScript.js"></script>
    </head>
    <body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="Home.php">REVA UNIVERSITY</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                      <!--<li class="active"><a href="#">HOME <span class="sr-only">(current)</span></a></li>
                      <li><a href="#">Something else here</a></li>-->

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" >HOME <span class="caret"></span></a>
                            <ul class="dropdown-menu multi-level">
                                <li>
                                    <a href="InsertTab.php">INSERT DATA</a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <a href="#">UPDATE DATA</a>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">DISCIPLINE <span class="caret"></span></a>
                            <ul class="dropdown-menu">

                                <!--ENGINEERING & TECHNOLOGY -> SubMenu Of DISCIPLINE-->
                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ENGINEERING and TECHNOLOGY</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="ShowProjectProposalTable.php">CIVIL ENGINEERING</a></li>
                                        <li><a href="#">COMPUTER and TECHNOLOGY</a></li>
                                        <li><a href="#">ELECTRICAL and ELECTRONICS ENGINEERING</a></li>
                                        <li><a href="#">ELECTRONIC and COMMUNICATION ENGINEERING</a></li>
                                        <li><a href="#">MECHANICAL ENGINEERING</a></li>
                                        <li><a href="#">ARCHITECTURE ENGINEERING</a></li>
                                    </ul>
                                </li>
                                <li role="separator" class="divider"></li>

                                <!--SCIENCE & TECHNOLOGY -> SubMenu Of DISCIPLINE-->
                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SCIENCE and TECHNOLOGY</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">PHYSICAL SCIENCE and COMPUTER APPLICATION</a></li>
                                        <li><a href="#">CHEMICAL and BIOLOGICAL SCIENCE</a></li>
                                        <li><a href="#">BIO-CHEMISTRY</a></li>
                                        <li><a href="#">CHEMISTRY</a></li>
                                        <li><a href="#">MATHEMATICS</a></li>
                                        <li><a href="#">PHYSICS</a></li>
                                        <li><a href="#">STATISTICS</a></li>
                                    </ul>
                                </li>
                                <li role="separator" class="divider"></li>

                                <!--ARTS, HUMANITIES & EDUCATION -> SubMenu Of DISCIPLINE-->
                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ARTS, HUMANITIES and EDUCATION</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">KANNADA</a></li>
                                        <li><a href="#">ENGLISH</a></li>
                                        <li><a href="#">HINDI</a></li>
                                        <li><a href="#">EDUCATION</a></li>
                                    </ul>
                                </li>
                                <li role="separator" class="divider"></li>

                                <!--LEGAL STUDIES MENU-->
                                <li> <a href="#">LEGAL STUDIES</a> </li>
                                <li role="separator" class="divider"></li>

                                <!--COMMERCE & MANAGEMENT STUDIES -> SubMenu Of DISCIPLINE-->
                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" >COMMERCE and MANAGEMENT</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">COMMERCE</a></li>
                                        <li><a href="#">ECONOMICS</a></li>
                                        <li><a href="#">LIBRARY and INFORMATION SCIENCE</a></li>
                                        <li><a href="#">MANAGEMENT STUDIES</a></li>
                                    </ul>
                                </li>

                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SEARCH <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">REPORT PENDING</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">FEE PENDING</a></li>
                            </ul>
                        </li>

                        <!--HELP MENU-->
                        <li><a href="#">HELP <span class="sr-only">(current)</span></a></li>
                        <!--Show Data-->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" >SHOW DATA<span class="caret"></span></a>
                            <ul class="dropdown-menu multi-level">
                                <li>
                                  <a href="ShowProjectProposalDataTable.php">PROJECT PROPOSALS</a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                  <a href="ShowFacultyMembersNotYetRegisteredDataTable.php">FACULTY MEMBERS NOT YET REGISTERED</a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                  <a href="ShowFacultyMembersPublicationPlanDataTable.php">FACULTY MEMBERS PUBLICATION PLAN</a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                  <a href="ShowUniversityHelpDataTable.php">UNIVERSITY HELP</a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                  <a href="ShowContactHelpDataTable.php">CONTACT HELP</a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                  <a href="ShowPublicationAYearDataTable.php">PUBLICATION (ALL YEAR)</a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                  <a href="ShowJournalSubmissionYearDataTable.php">JOURNAL (PER YEAR)</a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                  <a href="#">JOURNALS</a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                  <a href="ShowBookDataTable.php">BOOK</a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                  <a href="#">PATENTS</a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                  <a href="#">ACTION ITEM FROM SCHOOL</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <form class="navbar-form navbar-right" action="logout.php" method="post">
                        <!--<div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>-->

                        <button type="submit" name="submit" class="btn btn-danger">Logout</button>
                    </form>
                    <!--  <ul class="nav navbar-nav navbar-right">
                          <li><a href="#" name="submit" >LOGOUT</a></li>
                      </ul> -->
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <br /><br />

        <!--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
        <!--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
        <!--//////////////////////////////////////////////////NAV OVER TABLE STARTED///////////////////////////////////////////////////////-->
        <!--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
        <!--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
        <!--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
        <div class="container-fluid">
             <h3 align="center"  id="Heading">Show Data</h3>
             <br />
             <hr>
             <script>
               $(document).ready(function(){
                 $("#discipline").on('change',function(){
                     var dis = $("#discipline").val();
                     $.ajax({
                         url: 'DBFetchFolder/ShowDBookFetchData.php',
                         type: 'POST',
                         data: 'request='+ dis,
                         success:function(data){
                           $("#table-container").html(data);
                         },
                       });
                     });
                     $("#branch").on('change',function(){
                         var branch = $("#branch").val();
                         $.ajax({
                             url: 'DBFetchFolder/ShowBBookFetchData.php',
                             type: 'POST',
                             data: 'requesto='+ branch,
                             success:function(data){
                               $("#table-container").html(data);
                             },
                           });
                         });
               });
             </script>

             <!--DISCIPLINE COMBOBOX-->
             <div class="container">
                 <div class="form-inline">
                     <label class="control-label col-sm-2" id="DLabel" for="DisciplineCB">Discipline</label>
                     <div class="col-sm-4">
                         <select class="form-control" name="discipline" id="discipline" onchange="populate('discipline', 'branch')">
                             <option value="Discipline">Discipline</option>
                             <option value="ENGINEERING and TECHNOLOGY">ENGINEERING &amp; TECHNOLOGY</option>
                             <option value="SCIENCE and TECHNOLOGY">SCIENCE &amp; TECHNOLOGY</option>
                             <option value="ARTS, HUMANITIES and EDUCATION">ARTS, HUMANITIES &amp; EDUCATION</option>
                             <option value="LEGAL STUDIES">LEGAL STUDIES</option>
                             <option value="COMMERCE and MANAGEMENT">COMMERCE and MANAGEMENT</option>
                         </select>
                     </div>
                 </div>

                 <!--BRANCH COMBOBOX-->
                 <div class="form-inline text-right">
                     <label class="control-label col-sm-2" id="BLabel" for="BranchCB">Branch</label>
                     <div class="col-sm-2">
                         <select class="form-control" name="branch" id="branch">
                             <option value="Branch">Branch</option>
                         </select>
                     </div>
                 </div>

             </div>
             </div>
             <hr>

        <div class="container-fluid">
            <h3 align="center">Book</h3>
            <br />
            <div class="table-responsive" id="table-container">
              <?php
              $connect = mysqli_connect("localhost", "root", "", "RMARS");
              $query = "SELECT * FROM Book ORDER BY ID Asc";
              $result = mysqli_query($connect, $query);
              ?>
              <table id="employee_data" class="table table-striped table-bordered table-hover">
                  <thead>
                      <tr>
                          <td>Discipline</td>
                          <td>Branch</td>
                          <td>Author</td>
                          <td>Title</td>
                          <td>Chapter</td>
                          <td>Year</td>
                          <td>Publisher</td>
                          <td>ISBN</td>

                      </tr>
                  </thead>
                  <?php
                  while ($row = mysqli_fetch_array($result)) {
                      echo '
                                      <tr>
                                           <td>' . $row["Discipline"] . '</td>
                                           <td>' . $row["Branch"] . '</td>
                                           <td>' . $row["Author"] . '</td>
                                           <td>' . $row["Title"] . '</td>
                                           <td>' . $row["Chapter"] . '</td>
                                           <td>' . $row["Year"] . '</td>
                                           <td>' . $row["Publisher"] . '</td>
                                           <td>' . $row["ISBN"] . '</td>
                                      </tr>
                                      ';
                  }
                  ?>
                </table>
                <br><br><br>
            </div>
        </div>


    </body>
</html>


<script>
    $(document).ready(function () {
        $('#employee_data').DataTable();

    });
</script>
