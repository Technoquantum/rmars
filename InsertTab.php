<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>WELCOME | RMARS</title>
        <meta charset="utf-8">
        <title>RMARS | Insert</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
        <link rel="stylesheet" href="CSS/style.css">




        <link rel="stylesheet" href="CSS/InsertTabStyle.css">
        <script type="text/javascript" src="InsertTabsScript.js"></script>
        <script type="text/javascript" src="JavaScript/ProjectProposalJS.js"></script>
        <script type="text/javascript" src="JavaScript/FacultyMembersNotRegistered.js"></script>
        <script type="text/javascript" src="JavaScript/FacultyMemebersPublication.js"></script>
        <script type="text/javascript" src="JavaScript/UniversityHelp.js"></script>
        <script type="text/javascript" src="JavaScript/ContactHelp.js"></script>
        <script type="text/javascript" src="JavaScript/SPSubmitConference.js"></script>
        <script type="text/javascript" src="JavaScript/SPSubmitJournal.js"></script>
        <script type="text/javascript" src="JavaScript/SubmissionJournal.js"></script>
        <script type="text/javascript" src="JavaScript/Book.js"></script>


        <script type="text/javascript" src="InsertTabsScriptRestrictFields.js"></script>
        <style type="text/css">
            /* To Dropdown navbar dropdown on hover */
            .navbar-nav > li:hover > .dropdown-menu {
                display: block;
                background-color: #000;
            }
            .dropdown-submenu {
                position: relative;
            }
            .dropdown-menu a {
                color: #000;
            }

            .dropdown-submenu>.dropdown-menu {
                top: 0;
                left: 100%;
                margin-top: -6px;
                margin-left: -1px;
                -webkit-border-radius: 0 6px 6px 6px;
                -moz-border-radius: 0 6px 6px;
                border-radius: 0 6px 6px 6px;
                background-color: #000;
            }

            .dropdown-submenu:hover>.dropdown-menu {
                display: block;
            }

            .dropdown-submenu>a:after {
                display: block;
                content: " ";
                float: right;
                width: 0;
                height: 0;
                border-color: transparent;
                border-style: solid;
                border-width: 5px 0 5px 5px;
                border-left-color: #ccc;
                margin-top: 5px;
                margin-right: -10px;

            }

            .dropdown-submenu:hover>a:after {
                border-left-color: #fff;
            }

            .dropdown-submenu.pull-left {
                float: none;
            }

            .dropdown-submenu.pull-left>.dropdown-menu {
                left: -100%;
                margin-left: 10px;
                -webkit-border-radius: 6px 0 6px 6px;
                -moz-border-radius: 6px 0 6px 6px;
                border-radius: 6px 0 6px 6px;

            }
        </style>
    </head>
    <body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
        <?php
        if (isset($_SESSION['username'])) {
            ?>
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="Home.php">REVA UNIVERSITY</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                          <!--<li class="active"><a href="#">HOME <span class="sr-only">(current)</span></a></li>
                          <li><a href="#">Something else here</a></li>-->

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" >HOME <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-level">
                                    <li>
                                        <a href="InsertTab.php">INSERT DATA</a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="#">UPDATE DATA</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">DISCIPLINE <span class="caret"></span></a>
                                <ul class="dropdown-menu">

                                    <!--ENGINEERING & TECHNOLOGY -> SubMenu Of DISCIPLINE-->
                                    <li class="dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ENGINEERING and TECHNOLOGY</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="ShowProjectProposalTable.php">CIVIL ENGINEERING</a></li>
                                            <li><a href="#">COMPUTER and TECHNOLOGY</a></li>
                                            <li><a href="#">ELECTRICAL and ELECTRONICS ENGINEERING</a></li>
                                            <li><a href="#">ELECTRONIC and COMMUNICATION ENGINEERING</a></li>
                                            <li><a href="#">MECHANICAL ENGINEERING</a></li>
                                            <li><a href="#">ARCHITECTURE ENGINEERING</a></li>
                                        </ul>
                                    </li>
                                    <li role="separator" class="divider"></li>

                                    <!--SCIENCE & TECHNOLOGY -> SubMenu Of DISCIPLINE-->
                                    <li class="dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SCIENCE and TECHNOLOGY</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">PHYSICAL SCIENCE and COMPUTER APPLICATION</a></li>
                                            <li><a href="#">CHEMICAL and BIOLOGICAL SCIENCE</a></li>
                                            <li><a href="#">BIO-CHEMISTRY</a></li>
                                            <li><a href="#">CHEMISTRY</a></li>
                                            <li><a href="#">MATHEMATICS</a></li>
                                            <li><a href="#">PHYSICS</a></li>
                                            <li><a href="#">STATISTICS</a></li>
                                        </ul>
                                    </li>
                                    <li role="separator" class="divider"></li>

                                    <!--ARTS, HUMANITIES & EDUCATION -> SubMenu Of DISCIPLINE-->
                                    <li class="dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ARTS, HUMANITIES and EDUCATION</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">KANNADA</a></li>
                                            <li><a href="#">ENGLISH</a></li>
                                            <li><a href="#">HINDI</a></li>
                                            <li><a href="#">EDUCATION</a></li>
                                        </ul>
                                    </li>
                                    <li role="separator" class="divider"></li>

                                    <!--LEGAL STUDIES MENU-->
                                    <li> <a href="#">LEGAL STUDIES</a> </li>
                                    <li role="separator" class="divider"></li>

                                    <!--COMMERCE & MANAGEMENT STUDIES -> SubMenu Of DISCIPLINE-->
                                    <li class="dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" >COMMERCE and MANAGEMENT</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">COMMERCE</a></li>
                                            <li><a href="#">ECONOMICS</a></li>
                                            <li><a href="#">LIBRARY and INFORMATION SCIENCE</a></li>
                                            <li><a href="#">MANAGEMENT STUDIES</a></li>
                                        </ul>
                                    </li>

                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SEARCH <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">REPORT PENDING</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#">FEE PENDING</a></li>
                                </ul>
                            </li>

                            <!--HELP MENU-->
                            <li><a href="#">HELP <span class="sr-only">(current)</span></a></li>

                            <!--Show Data-->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" >SHOW DATA<span class="caret"></span></a>
                                <ul class="dropdown-menu multi-level">
                                    <li>
                                        <a href="ShowProjectProposalDataTable.php">PROJECT PROPOSALS</a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="ShowFacultyMembersNotYetRegisteredDataTable.php">FACULTY MEMBERS NOT YET REGISTERED</a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="ShowFacultyMembersPublicationPlanDataTable.php">FACULTY MEMBERS PUBLICATION PLAN</a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="ShowUniversityHelpDataTable.php">UNIVERSITY HELP</a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="ShowContactHelpDataTable.php">CONTACT HELP</a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="ShowPublicationAYearDataTable.php">PUBLICATION (ALL YEAR)</a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="ShowJournalSubmissionYearDataTable.php">JOURNAL (PER YEAR)</a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="#">JOURNALS</a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="ShowBookDataTable.php">BOOK</a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="#">PATENTS</a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="#">ACTION ITEM FROM SCHOOL</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <form class="navbar-form navbar-right" >
                            <!--<div class="form-group">
                                <input type="text" class="form-control" placeholder="Search">
                            </div>-->

                            <button type="button" id="logout" name="submit" class="btn btn-danger">Logout</button>
                        </form>
                        <!--  <ul class="nav navbar-nav navbar-right">
                              <li><a href="#" name="submit" >LOGOUT</a></li>
                          </ul> -->
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>


            <!--//////////////////////////////////DYNAMIC TABS For Inserting Data in Fields for the table-->


            <div class="container-fluid" id="con">
                <h2 class="text-center">Insert Data</h2>
                <hr>

                <!--DISCIPLINE COMBOBOX-->
                <div class="container">
                    <div class="form-inline">

                        <div class="col-sm-6">
                            <select class="form-control" name="discipline" id="discipline" onchange="populate('discipline', 'branch')">
                                <option value="Discipline">Discipline</option>
                                <option value="ENGINEERING and TECHNOLOGY">ENGINEERING &amp; TECHNOLOGY</option>
                                <option value="SCIENCE and TECHNOLOGY">SCIENCE &amp; TECHNOLOGY</option>
                                <option value="ARTS, HUMANITIES and EDUCATION">ARTS, HUMANITIES &amp; EDUCATION</option>
                                <option value="LEGAL STUDIES">LEGAL STUDIES</option>
                                <option value="COMMERCE and MANAGEMENT">COMMERCE and MANAGEMENT</option>
                            </select>
                        </div>
                    </div>

                    <!--BRANCH COMBOBOX-->
                    <div class="form-inline text-right">

                        <div class="col-sm-6">
                            <select class="form-control" name="branch" id="branch">
                                <option value="Branch">Branch</option>
                            </select>
                        </div>
                    </div>
                </div>

                <hr>

                <ul class="nav nav-tabs">

                    <li class="active"><a data-toggle="tab" href="#home">Project Proposals</a></li>
                    <li><a data-toggle="tab" href="#menu1">Action Plan by Faculty members who are yet to register for Ph.D</a></li>
                    <li><a data-toggle="tab" href="#menu2">Action Plan by Faculty members for Publication</a></li>
                    <li><a data-toggle="tab" href="#menu3">List the help you may need from the university in achieving the set target</a></li>
                    <li><a data-toggle="tab" href="#menu4">List the contacts who you know to help the school to procure grants</a></li>
                    <li><a data-toggle="tab" href="#menu5">Submission of Publication (All Year)</a></li>
                    <li><a data-toggle="tab" href="#menu6">Submission of Journal (Per Year)</a></li>
                    <li><a data-toggle="tab" href="#menu7">Journals</a></li>
                    <li><a data-toggle="tab" href="#menu8">Book</a></li>
                    <li><a data-toggle="tab" href="#menu9">Patent</a></li>
                    <li><a data-toggle="tab" href="#menu10">Action item from school</a></li>

                </ul>
            </div>
            <div class="container text-center">


                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <h3>Project Proposals</h3>

                        <!--FORM For Project Proposals (year wise submission)-->
                        <form id="PPForm" class="form-horizontal" method="post" action="PHP/InsertProjectProposalData.php" >
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="Project">Project Title </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="ProTitle" placeholder="Enter Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="Year">Year </label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="PPYear" id="PPYear">
                                        <option value="Year">Year</option>
                                        <?php
                                        foreach (range(2014, (int) date("Y")) as $year) {
                                            echo "\t<option value='" . $year . "'>" . $year . "</option>\n\r";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="Principle">Name of Principle </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="PrincName" placeholder="Enter Principle Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="Investigator">Name of Investigator </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="InvesName" placeholder="Enter Investigator Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="Co-Investigator">Name of Co-Investigator </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="CoInvestName" placeholder="Enter Co-Investigator Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="Funding Agency/Scheme">Funding Agency/Scheme </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="FAScheme" placeholder="Funding Agency/Scheme">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="Amount">Amount</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="AmountNumber" placeholder="Amount in Rs.">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="Status">Status</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="PPStatus" placeholder="Status">
                                </div>
                            </div>
                            <!--<div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="checkbox">
                                        <label><input type="checkbox"> Remember me</label>
                                    </div>
                                </div>
                            </div>-->
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" name="PPsubmit" id="PPSubmit" class="btn btn-success">Submit</button>
                                </div>
                            </div> <!--onclick="ProjectProposal();"-->

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10" id="Message"></div>
                            </div>
                        </form>


                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <h3>Action Plan by Faculty Members who are yet to register for Ph.D</h3>
                        <!--FORM For Faculty Members who are yet to register for Ph.D-->
                        <form id="FNRForm" class="form-horizontal" method="POST" action="PHP/InsertFacultyNotRegisteredData.php">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="Faculty">Faculty Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="FNRFaculty" placeholder="Name of Faculty">
                                </div>
                            </div>

                            <div class="form-group inline">
                                <label class="control-label col-sm-2" for="FNRDate">Date </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="FNRDay" id="FNRDay">
                                        <option value="Day">Day</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" name="FNRMonth" id="FNRMonth">
                                        <option value="Month">Month</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" name="FNRYear" id="FNRYear">
                                        <option value="Year">Year</option>
                                        <?php
                                        foreach (range(2014, (int) date("Y")) as $year) {
                                            echo "\t<option value='" . $year . "'>" . $year . "</option>\n\r";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="FNRStatus">Status </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="FNRStatus" placeholder="Enter Paper Status">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" name="submit" id="FNRSubmit" class="btn btn-success">Submit</button>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10" id="FNRMessage"></div>
                            </div>
                        </form>
                    </div>
                    <div id="menu2" class="tab-pane fade">

                        <h3>Action Plan by Faculty Members for Publications</h3>

                        <!--FORM For Faculty Members for Publications-->
                        <form id="FMPForm" class="form-horizontal" method="POST" action="PHP/InsertFacultyPublicationData.php">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="FMPFaculty">Faculty Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="FMPFaculty" placeholder="Name of Faculty">
                                </div>
                            </div>

                            <div class="form-group inline">
                                <label class="control-label col-sm-2" for="FMPDate">Date</label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="FMPDay" id="FMPDay">
                                        <option value="Day">Day</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" name="FMPMonth" id="FMPMonth">
                                        <option value="Month">Month</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" name="Year" id="FMPYear">
                                        <option value="Year">Year</option>
                                        <?php
                                        foreach (range(2014, (int) date("Y")) as $year) {
                                            echo "\t<option value='" . $year . "'>" . $year . "</option>\n\r";
                                        }
                                        ?>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="FMPStatus">Status </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="FMPStatus" placeholder="Enter Paper Status">
                                </div>
                            </div>

                            <div class="form-group inline">
                                <label class="control-label col-sm-2" for="FMPRDate">Research Article Submission</label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="FMPRDay" id="FMPRDay">
                                        <option value="Day">Day</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" name="FMPRMonth" id="FMPRMonth">
                                        <option value="Month">Month</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" name="FMPRYear" id="FMPRYear">
                                        <option value="Year">Year</option>
                                        <?php
                                        foreach (range(2014, (int) date("Y")) as $year) {
                                            echo "\t<option value='" . $year . "'>" . $year . "</option>\n\r";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" name="submit" id="FMPSubmit" class="btn btn-success">Submit</button>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10" id="FMPMessage"></div>
                            </div>
                        </form>
                    </div>

                    <div id="menu3" class="tab-pane fade">
                        <h3>List of help you may need from the university in achieving the set target</h3>

                        <!--FORM For University Help-->
                        <form id="UHForm" class="form-horizontal" method="POST" action="PHP/InsertUniversityHelpData.php">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="UHRequirement">Requirements</label>
                                <div class="col-sm-10">
                                    <textarea name="UHRequirement" class="form-control" id="UHRequirement" placeholder="Enter Requirements" rows="8" cols="60"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" name="submit" id="UHSubmit" class="btn btn-success">Submit</button>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10" id="UHMessage"></div>
                            </div>

                        </form>
                    </div>



                    <div id="menu4" class="tab-pane fade">
                        <h3>List the contacts who you know to help the school to procure grants</h3>

                        <!--FORM For Contact Help For School-->

                        <form id="CHForm" method="post" action="PHP/InsertContactHelpData.php" class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="CHName">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="CHName" placeholder="Enter Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="CHOrg">Organization</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="CHOrg" placeholder="Enter Organization">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" name="submit" id="CHSubmit" class="btn btn-success">Submit</button>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10" id="CHMessage"></div>
                            </div>
                        </form>
                    </div>

                    <div id="menu5" class="tab-pane fade">
                        <h3>Submission of Publication (All Year)</h3>

                        <!--FORM For Publication Submission-->

                        <div class="container-fluid">
                            <form id="JSPForm" method="post" action="PHP/InsertSPJournalData.php" class="form-horizontal">
                                <!--Journal-->
                                <div class="col-sm-6 text-center">
                                    <h4 id="JournalHeading">Journal</h4>

                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="JSPYear">Year</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name = "JSPYear" id="JSPYear">
                                                <option value="Year">Year</option>
                                                <?php
                                                foreach (range(2014, (int) date("Y")) as $year) {
                                                    echo "\t<option value='" . $year . "'>" . $year . "</option>\n\r";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="JSPWebScience">Web of Science</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="JSPWebScience" placeholder="Enter Web of Science Publication">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="JSPScopus">Scopus</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="JSPScopus" placeholder="Enter Scopus">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="SPGoogleSch">Google Scholars</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="JSPGoogleSch" placeholder="Enter Google Scholars">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="JSPIndi">Indiancited</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="JSPIndi" placeholder="Enter Indiancited">
                                        </div>
                                    </div>


                                    <!--Button and Message Box-->
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" name="submit" class="btn btn-success" id="JSPSubmit">Submit Journal</button>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10" id="JSPMessage"> </div>
                                    </div>
                                    <hr class="lineunderJournalButton">
                                </div>
                            </form>

                            <form id="CSPForm" method="post" action="PHP/InsertSPConferenceData.php" class="form-horizontal">
                                <!--Conference-->

                                <div class="col-sm-6 text-center">
                                    <h4 id="ConferenceHeading">Conference</h4>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="CSPYear">Year</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="CSPYear" id="CSPYear">
                                                <option value="Year">Year</option>
                                                <?php
                                                foreach (range(2014, (int) date("Y")) as $year) {
                                                    echo "\t<option value='" . $year . "'>" . $year . "</option>\n\r";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="CSPWebScience">Web of Science</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="CSPWebScience" placeholder="Enter Web of Science Publication">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="CSPScopus">Scopus</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="CSPScopus" placeholder="Enter Scopus">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="CSPGoogleSch">Google Scholars</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="CSPGoogleSch" placeholder="Enter Google Scholars">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="CSPIndi">Indiancited</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="CSPIndi" placeholder="Enter Indication">
                                        </div>
                                    </div>

                                    <!--Button and Message Box-->
                                    <div class="col-sm-6 text-center">
                                        <div class="form-group">
                                            <div class="col-sm-offset-9 col-sm-10">
                                                <button type="submit" name="submit" id="CSPSubmit" class="btn btn-success">Submit Conference</button>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-9 col-sm-10" id="CSPMessage"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>

                    <div id="menu6" class="tab-pane fade">
                        <h3>Submission of Journal (Per Year)</h3>

                        <!--FORM For Contact Help For School-->

                        <form id="SJForm" method="POST" action="PHP/InsertSubmissionJournal.php" class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SJAuthor">Author</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="SJAuthor" placeholder="Enter Author">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SJTitle">Title</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="SJTitle" placeholder="Enter Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SJJournal">Journal</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="SJJournal" placeholder="Enter Journal">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SJVolume">Volume</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="SJVolume" placeholder="Enter Volume">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SJDate">Issue Date</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="SJIssueDate" id="SJIssueDate">
                                        <option value="Day">Day</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SJPage">Page</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="SJPage" placeholder="Enter Page">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SJMonth">Month</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="SJMonth" id="SJMonth">
                                        <option value="Month">Month</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SJYear">Year</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="SJYear" id="SJYear">
                                        <option value="Year">Year</option>
                                        <?php
                                        foreach (range(2014, (int) date("Y")) as $year) {
                                            echo "\t<option value='" . $year . "'>" . $year . "</option>\n\r";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SJScope">Scope</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="SJScope" id="SJScope">
                                        <option value="Scope">Choose</option>
                                        <option value="National">National</option>
                                        <option value="International">International</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SJIssn">ISSN</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="SJIssn" placeholder="Enter ISSN">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SJIndex">Index/Unindex</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="SJIndex" id="SJIndex" onchange="altering('SJIndex', 'SJIndexing'); ShowHideIndexFields()">
                                        <option value="IU">Choose</option>
                                        <option value="Indexed">Indexed</option>
                                        <option value="Unindexed">Unindexed</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group" id="SJIndiForm" style="display: none;">
                                <label class="control-label col-sm-2" for="SJIndexing">Indexed In</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="SJIndexing" id="SJIndexing">
                                        <option value="Web Of Science">Web Of Science</option>
                                        <option value="Scopus">Scopus</option>
                                        <option value="Indiancited">Indiancited</option>
                                        <option value="UGC">UGC</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6 text-center">
                                <div class="form-group">
                                    <div class="col-sm-offset-9 col-sm-10">
                                        <button type="submit" name="submit" id="SJSubmit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10" id="SJMessage"></div>
                            </div>
                        </form>
                    </div>

























                    <div id="menu7" class="tab-pane fade">

                    </div>




                    <div id="menu8" class="tab-pane fade">
                        <h3>Book</h3>

                        <!--FORM For Book-->
                        <form id="BForm" method="POST" action="PHP/InsertBookData.php" class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="BAuthor">Author</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="BAuthor" placeholder="Enter Author">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="BTitle">Title</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="BTitle" placeholder="Enter Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="BChapter">Chapter</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="BChapter" placeholder="Enter Chapter">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="BYear">Year</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="BYear" id="BYear">
                                        <option value="Year">Year</option>
                                        <?php
                                        foreach (range(2014, (int) date("Y")) as $year) {
                                            echo "\t<option value='" . $year . "'>" . $year . "</option>\n\r";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="BPublisher">Publisher</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="BPublisher" placeholder="Enter Publisher Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="BIsbn">ISBN</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="BIsbn" placeholder="Enter ISBN">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" name="submit" id="BSubmit" class="btn btn-success">Submit</button>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10" id="BMessage"></div>
                            </div>
                        </form>
                    </div>






                    <div id="menu9" class="tab-pane fade">

                    </div>

                    <div id="menu10" class="tab-pane fade">
                        <h3>Menu 3</h3>
                        <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                    </div>
                </div>
            </div>


            <!--Footer-->
            <footer class="container-fluid text-center">
                <a href="#myPage" title="To Top">
                    <span class="glyphicon glyphicon-chevron-up"></span>
                </a>
                <p><a href="InsertTab.php" title="Visit RMARS">www.RMARS.com</a></p>
            </footer>
            <?php
        } else {

            header("location: index.php");
        }
        ?>
        <script>
            $(document).ready(function () {
                // Add smooth scrolling to all links in navbar + footer link
                $(".navbar a, footer a[href='#myPage']").on('click', function (event) {
                    // Make sure this.hash has a value before overriding default behavior
                    if (this.hash !== "") {
                        // Prevent default anchor click behavior
                        event.preventDefault();

                        // Store hash
                        var hash = this.hash;

                        // Using jQuery's animate() method to add smooth page scroll
                        // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
                        $('html, body').animate({
                            scrollTop: $(hash).offset().top
                        }, 500, function () {

                            // Add hash (#) to URL when done scrolling (default click behavior)
                            window.location.hash = hash;
                        });
                    } // End if
                });
            })
        </script>
        <script>

            window.onload = function () {
                document.getElementById("SJIndiForm").style.display = 'none';
            }

            function altering(d, b) {
                var d = document.getElementById(d);
                var b = document.getElementById(b);

                b.innerHTML = "";

                if (d.value == "IU") {
                    var optionArray = ["|"];
                } else if (d.value == "Indexed") {
                    var optionArray = ["Web of Science|Web of Science",
                        "Scopus|Scopus",
                        "UGC|UGC",
                        "Indiancited|Indiancited"];
                } else if (d.value == "Unindexed") {
                    var optionArray = ["No Indexing|No Indexing"];
                }





                for (var option in optionArray) {
                    var pair = optionArray[option].split("|");
                    var newOption = document.createElement("option");
                    newOption.value = pair[0];
                    newOption.innerHTML = pair[1];
                    b.options.add(newOption);
                }
            }

            function ShowHideIndexFields() {

                var UIndex = document.getElementById("SJIndex").value;

                if (UIndex == "Choose") {

                    document.getElementById("SJIndiForm").style.display = 'none';

                } else if (UIndex == "Indexed") {
                    document.getElementById("SJIndiForm").style.display = '';
                } else {
                    document.getElementById("SJIndiForm").style.display = 'none';
                }
            }
        </script>

    </body>
</html>
<script>
    $(document).ready(function () {
        $('#employee_data').DataTable();
    });
</script>

<script>
    $(document).ready(function () {
        $('#login_button').click(function () {
            var username = $('#username').val();
            var password = $('#password').val();
            if (username != '' && password != '')
            {
                $.ajax({
                    url: "LoginAction.php",
                    method: "POST",
                    data: {username: username, password: password},
                    success: function (data)
                    {
                        //alert(data);
                        if (data == 'No')
                        {
                            alert("Wrong Data");
                        } else
                        {
                            $('#loginModal').hide();
                            location.reload();
                        }
                    }
                });
            } else
            {
                alert("Both Fields are required");
            }
        });
        $('#logout').click(function () {
            var action = "logout";
            $.ajax({
                url: "LoginAction.php",
                method: "POST",
                data: {action: action},
                success: function ()
                {
                    location.reload();
                }
            });
        });
    });
</script>
