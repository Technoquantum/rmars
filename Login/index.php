<?php
 session_start();
 ?>
 <!DOCTYPE html>
 <html>
      <head>
           <title>Webslesson Tutorial | Make Login Form by Using Bootstrap Modal with PHP Ajax Jquery</title>
           <meta name="viewport" content="width=device-width, initial-scale=1">
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
           <link href="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
           <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
           <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
           <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />

           <link rel="stylesheet" href="style.css">
           <link rel="stylesheet" href="InsertTabStyle.css">
           <script type="text/javascript" src="InsertTabsScript.js"></script>
           <script type="text/javascript" src="InsertTabsScriptRestrictFields.js"></script>
           <style type="text/css">
           /* To Dropdown navbar dropdown on hover */
       .navbar-nav > li:hover > .dropdown-menu {
           display: block;
           background-color: #000;
       }
       .dropdown-submenu {
           position: relative;
         }
       .dropdown-menu a {
         color: #000;
       }

       .dropdown-submenu>.dropdown-menu {
           top: 0;
           left: 100%;
           margin-top: -6px;
           margin-left: -1px;
           -webkit-border-radius: 0 6px 6px 6px;
           -moz-border-radius: 0 6px 6px;
           border-radius: 0 6px 6px 6px;
           background-color: #000;
       }

       .dropdown-submenu:hover>.dropdown-menu {
           display: block;
       }

       .dropdown-submenu>a:after {
           display: block;
           content: " ";
           float: right;
           width: 0;
           height: 0;
           border-color: transparent;
           border-style: solid;
           border-width: 5px 0 5px 5px;
           border-left-color: #ccc;
           margin-top: 5px;
           margin-right: -10px;

       }

       .dropdown-submenu:hover>a:after {
           border-left-color: #fff;
       }

       .dropdown-submenu.pull-left {
           float: none;
       }

       .dropdown-submenu.pull-left>.dropdown-menu {
           left: -100%;
           margin-left: 10px;
           -webkit-border-radius: 6px 0 6px 6px;
           -moz-border-radius: 6px 0 6px 6px;
           border-radius: 6px 0 6px 6px;

       }
           </style>
      </head>
    <body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
           <br />
           <div class="container" style="width:700px;">

                <?php
                if(isset($_SESSION['username']))
                {
                ?>
                <div align="center">
                  <button type="button" id="logout" name="submit" class="btn btn-danger">Logout</button>
                </div>
                <?php
                }
                else
                {
                ?>
                <h1>REVA Management Activity Research Service (RMARS)</h1>
                <br><br>
                <div class="container">

                    <!--Login Link-->
                    <div class="col-sm-6"  id="loginLink">
                        <a href="#" data-toggle="modal" data-target="#loginModal">Login</a>
                    </div>

                    <!--Signup Link-->
                    <div class="col-sm-6" id="signupLink">
                        <a href="#">Signup</a>
                    </div>

                </div>
                <?php
                }
                ?>
           </div>
           <br />
      </body>
 </html>
 <div id="loginModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
   <!-- Modal content-->
           <div class="modal-content">
                <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title">Login</h4>
                </div>
                <div class="modal-body">
                     <label>Username</label>
                     <input type="text" name="username" id="username" class="form-control" />
                     <br />
                     <label>Password</label>
                     <input type="password" name="password" id="password" class="form-control" />
                     <br />
                     <button type="button" name="login_button" id="login_button" class="btn btn-warning">Login</button>
                </div>
           </div>
      </div>
 </div>
 <script>
 $(document).ready(function(){
      $('#login_button').click(function(){
           var username = $('#username').val();
           var password = $('#password').val();
           if(username != '' && password != '')
           {
                $.ajax({
                     url:"action.php",
                     method:"POST",
                     data: {username:username, password:password},
                     success:function(data)
                     {
                          //alert(data);
                          if(data == 'No')
                          {
                               alert("Wrong Data");
                          }
                          else
                          {
                               $('#loginModal').hide();
                               location.reload();
                          }
                     }
                });
           }
           else
           {
                alert("Both Fields are required");
           }
      });
      $('#logout').click(function(){
           var action = "logout";
           $.ajax({
                url:"action.php",
                method:"POST",
                data:{action:action},
                success:function()
                {
                     location.reload();
                }
           });
      });
 });
 </script>
